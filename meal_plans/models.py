from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()

    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meal_plans",
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans"
    )

    def __str__(self):
        return self.name + " by " + str(self.owner)


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    mealplan = models.ForeignKey(
        "MealPlan",
        related_name="ratings",
        on_delete=models.CASCADE,
    )
